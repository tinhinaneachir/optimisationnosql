package com.example.apirest.service;

import com.example.apirest.dto.ClickDto;
import com.example.apirest.model.Click;
import lombok.Data;

import java.util.List;


public interface ClickService {
    List<ClickDto> findAll();

    String save(ClickDto clickDto);
}
