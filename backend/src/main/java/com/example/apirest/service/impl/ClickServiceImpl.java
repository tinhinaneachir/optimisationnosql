package com.example.apirest.service.impl;

import com.example.apirest.dto.ClickDto;
import com.example.apirest.model.Click;
import com.example.apirest.repository.ClickRepository;
import com.example.apirest.service.ClickService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClickServiceImpl implements ClickService {

    @Autowired
    ClickRepository clickRepository;

    @Override
    public List<ClickDto> findAll() {
        List<Click> clicks =  clickRepository.findAll();
        if(clicks == null) return null;
        List<ClickDto> clicksDto = new ArrayList<ClickDto>();
        for (Click click:
            clicks ) {
            if(click.getInnerHtml() != null && !click.getInnerHtml().isBlank()) {
                ClickDto clickDto = new ClickDto();
                clickDto.setClickTime(click.getClickTime());
                clickDto.setCssSelector(click.getCssSelector());
                clickDto.setInnerHtml(click.getInnerHtml());
                clicksDto.add(clickDto);
            }
        }
        return clicksDto;
    }
    @Override
    public String save(ClickDto clickDto) {
        Click click = new Click();
        click.setClickTime(clickDto.getClickTime());
        click.setCssSelector(clickDto.getCssSelector());
        click.setInnerHtml(clickDto.getInnerHtml());
        Click saved = clickRepository.save(click);
        return saved.getId();
    }

}
