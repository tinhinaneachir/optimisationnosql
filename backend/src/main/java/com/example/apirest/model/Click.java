package com.example.apirest.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;


@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Document(collection = "clicks")
public class Click {
        @Id
        private String id;
        private String innerHtml;
        private String cssSelector;
        private String clickTime;


        @Override
        public String toString() {
                return "Click{" +
                        "id='" + id + '\'' +
                        ", innerHtml='" + innerHtml + '\'' +
                        ", cssSelector=" + cssSelector + '\'' +
                        ", clickTime=" + clickTime +
                        '}';
        }

}
