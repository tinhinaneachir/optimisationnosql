package com.example.apirest.controller;

import com.example.apirest.dto.ClickDto;
import com.example.apirest.service.ClickService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/click")
public class ClickController {
    @Autowired
    ClickService clickService;
        @PostMapping("/save")
        public String saveClick(@RequestBody ClickDto clickDto) {
            return clickService.save(clickDto);
        }

    @GetMapping("/all")
    public List<ClickDto> getAll(){
            return clickService.findAll();
        }

    }

