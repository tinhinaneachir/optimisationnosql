package com.example.apirest.repository;

import java.util.Optional;
import com.example.apirest.model.ERole;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.example.apirest.model.Role;

public interface RoleRepository extends MongoRepository<Role, String> {
    Optional<Role> findByName(ERole name);
}