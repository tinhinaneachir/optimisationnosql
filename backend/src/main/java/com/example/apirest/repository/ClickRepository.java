package com.example.apirest.repository;
import com.example.apirest.model.Click;
import lombok.Data;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClickRepository extends MongoRepository<Click, String> {

    List<Click> findAll();
}
