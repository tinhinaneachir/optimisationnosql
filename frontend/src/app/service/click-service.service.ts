import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Click} from "../model/click.model";
import * as Console from "console";
import {subscribeToArray} from "rxjs/internal/util/subscribeToArray";


const CLICK_API = 'http://localhost:8080/api/click/';


@Injectable({
  providedIn: 'root'
})
export class ClickServiceService {

   list2! : Click[] ;

  constructor(private  http: HttpClient) { }

  getClicks(auth_token:string): Observable<Click[]> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'/*,
        'Autorization': 'Bearer '+auth_token*/
      })
    };
    var list = this.http.get<Click[]>(CLICK_API+'all', httpOptions);

    //getNewClicks(list);


    return list;
  }

  getNewClicks(list:Observable<Click[]>): Click[] {


    return  [] ;
    //( (data) => {return data})
  }
}


