import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfirurationComponent } from './confiruration.component';

describe('ConfirurationComponent', () => {
  let component: ConfirurationComponent;
  let fixture: ComponentFixture<ConfirurationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfirurationComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfirurationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
