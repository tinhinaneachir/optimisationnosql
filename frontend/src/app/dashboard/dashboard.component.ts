import {Component, OnInit} from '@angular/core';
import {Data} from "@angular/router";
import {ClickServiceService} from "../service/click-service.service";
import {Click} from "../model/click.model";



@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  clicks!: Click[];
  histoClicks = new Map<string, number>();
  histoData: any;
  basicData: any;
  basicOptions: any;
  dataDashboard: any;
  chartOptions: any;


  constructor(private clickService: ClickServiceService) { }
  ngOnInit() {

    this.setData();

    this.basicData = {
      labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
      datasets: [
        {
          label: 'My First dataset',
          backgroundColor: '#42A5F5',
          dataDashboard: [65, 59, 80, 81, 56, 55, 40]
        },
        {
          label: 'My Second dataset',
          backgroundColor: '#FFA726',
          dataDashboard: [28, 48, 40, 19, 86, 27, 90]
        }
      ]
    };

    this.dataDashboard = {
      labels: ['A','B','C'],
      datasets: [
        {
          dataDashboard: [300, 50, 100],
          backgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ],
          hoverBackgroundColor: [
            "#FF6384",
            "#36A2EB",
            "#FFCE56"
          ]
        }
      ]
    };
  }

  setData(): void {
    var token = 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpemFuIiwiaWF0IjoxNjc4NTQ3ODU0LCJleHAiOjE2Nzg2MzQyNTR9.VQFt7Es1tdLjbRvJTJdX8wKP71NGdUnZyIeakGup7SWSO9Q6GaAotB4qSrv8Tz8avICeg7lKsHDbpaD4I4dUkQ';
    this.clickService.getClicks(token).subscribe(
      data => {
        this.setClicks(data);
        this.setHistoClicks(data);
      });
  }

  setClicks(clicks:Click[] ): void {
    this.clicks = clicks;
  }

  setHistoClicks(clicks:Click[] ): void {
    if (typeof clicks !== "undefined") {
      clicks.forEach((click) => {
        const key = click.innerHtml;
        if (this.histoClicks.has(key)) {
          const value = this.histoClicks.get(key);
          if (value) {
            this.histoClicks.set(key, (value + 1));
          }
        } else {
          this.histoClicks.set(key, 1);
        }
      });
      this.histoData = {
        labels: Array.from(this.histoClicks.keys()),
        datasets: [
          {
            label: 'Fréquence des clicks',
            backgroundColor: '#42A5F5',
            dataDashboard: Array.from(this.histoClicks.values())
          }
        ]
      };

    }
  }

}
