document.addEventListener('click', function (event) {
  var clientTime = new Date().toLocaleString();
  var cssSelector = getCssSelector(event.target);
  var innerText = event.target.innerText;

  console.log('Click event at ' + clientTime);
  console.log('CSS selector: ' + cssSelector);
  console.log('Inner text: ' + innerText);

  const click = {clickTime: clientTime, cssSelector: cssSelector, innerHtml: innerText};
  saveClick(click);
});

//const jwt = require('jsonwebtoken');

const authApiUrl = 'http://localhost:8080/api/auth/signin';

const credentials = {
  username: 'amchiche',
  password: '123456789'
};

function saveClick(click) {
  /*fetch(authApiUrl, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(credentials)
  })
    .then(response => response.json())
    .then(data => {
      const token = data.token;*/
      const clickApiUrl = 'http://localhost:8080/api/click/save';

      console.log('clickApiUrl ' + clickApiUrl);

      fetch(clickApiUrl, {
        method: 'POST',
        body: JSON.stringify(click),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer ' +'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJpemFuIiwiaWF0IjoxNjc4NTQzNTQ1LCJleHAiOjE2Nzg2Mjk5NDV9.bYtukmphGyx01u73H4qc3jaRB9W03zhvP39n4umMZfABNx30pkiyXhD-djXVU5URgPS7cD89tuFB8EDd_Ips0g'/*+ token*/
        }
      }).then(response => response.json())
        .then(click => {
          console.log('Success:', click);
        })
        .catch(error => {
          console.log('errrrror ');
          console.error('Error:', error);
        });
      window.addEventListener('popstate', function (event) {
        console.log('Page changed');
      });
   /* })
    .catch(error => console.error(error));*/
}

function getCssSelector(el) {
  var path = [];
  while (el.parentNode) {
    var tagName = el.tagName.toLowerCase();
    var siblings = el.parentNode.children;
    var nth = 1;
    for (var i = 0; i < siblings.length; i++) {
      if (siblings[i].tagName.toLowerCase() == tagName) {
        if (siblings[i] == el) {
          path.unshift(tagName + ':nth-child(' + nth + ')');
          break;
        }
        nth++;
      }
    }
    el = el.parentNode;
  }
  return path.join(' > ');
}









